import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {
  membersFaith: Array<any> = [
    {
      text: 'person 1',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    },
    {
      text: 'person 2',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    },
    {
      text: 'person 3',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    },
    {
      text: 'person 4',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    },
    {
      text: 'person 5',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    },
    {
      text: 'person 6',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    },
    {
      text: 'person 7',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    },
    {
      text: 'person 8',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    }
  ];
  
  membersPerseverance: Array<any> = [
    {
      text: 'person 1',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    },
    {
      text: 'person 2',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    },
    {
      text: 'person 3',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    },
    {
      text: 'person 4',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    }
  ];

  membersPatience: Array<any> = [
    {
      text: 'person 1',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    },
    {
      text: 'person 2',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    }
  ];

  membersMiracle: Array<any> = [
    {
      text: 'person 1',
      url: 'https://cdn2.iconfinder.com/data/icons/people-80/96/Picture1-512.png'
    }
  ];
  
  constructor() { }

  ngOnInit() {
  }

}
